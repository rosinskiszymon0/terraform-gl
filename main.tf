terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.9.1"
    }
  }
}
provider "gitlab" {
  token = "glpat-ZzSQMB7UWnwyUCZJHhC7"
}


data "gitlab_project" "example_project" {
  id = 55564710
}

resource "gitlab_project_variable" "sample_project_variable" {
  project = data.gitlab_project.example_project.id
  key     = "example_variable_unique"
  value   = "Greetings World!"
}
